import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from respuestas_diccionario import respuestas_formaA

FormaA=respuestas_formaA() ##Objeto con herramientas de calificacion forma A

def leer_imagen(nombre):
    imagen=cv2.imread(nombre,0)
    return imagen
def mostrar_imagen(img,op):
    if op==1:
        cv2.imshow("imagen",img)
        cv2.waitKey(0)
    else:
        plt.imshow(img)
        plt.xticks([]), plt.yticks([])
        plt.show()

def calificacion(img):
    return(np.linalg.norm(img,2))

def restart_ancho():
    return(50) ##ANCHO INIT INICIAL
def respuestas(img):
    marcas=range(17) ## NUMERO DE MARCAS NEGRAS EN FORMA A
    suma_negras=0 ##Variable para desfase de rango para marca negra
    alto=36 ##Dimensiones de la marca alto ##NO MODIFICAR
    ancho=90##Dimensiones de la marca ancho ##NO MODIFICAR

    ancho_end=200 ##Dimensiones de area con error para marca negra
    alto_end=1043  ##Dimensiones de area con error para marca negra

    altoinit=944 ##Coordenada Y para marca con rango de error
    anchoinit=50 ##Coordenada X para marca con rango de error
    
    ancho_marcas=50 ##Coordenada X para marca con rango de error
    anchoinit_max=0## Variable para establecer marca negra dentro de rango
    altoinit_max=0 ## Variable para establecer marca negra dentro de rango
    norma_min=100000 #VALOR PARA COMPARACION MINIMA
    salto_marca_negra=99 ## Salto entre pixeles de marcas negras
    distancia_respuesta=198 ##Distancia entre puntos para respuesta

    tamano_pregunta=15##Tamano del recuadro para captura de respuesta
    distancia_marca_respesta=349 ##Distancia entre la marca negra y la primera opcion de respuesta

    altura_actual_inicial=altoinit
    altura_actual_final=alto_end

    numero_pregunta=1


    ###SUMATORIA PARA MARCAS NEGRAS HACIA ABAJO
    for i in marcas:
        img = cv2.rectangle(img,(ancho_marcas,altura_actual_inicial),(ancho_end,altura_actual_final),(0,255,0),1)
        #BARRIDO EN Y
        while altura_actual_inicial+alto <= altura_actual_final:
           recorte=img[altura_actual_inicial:altura_actual_inicial+alto,anchoinit:anchoinit+ancho]
           norma=np.linalg.norm(recorte,2)
           if norma<norma_min:
               norma_min=norma
               altoinit_max=altura_actual_inicial ## Coordenada Y para maxima marca negra
           altura_actual_inicial=altura_actual_inicial+1

        
        #BARRIDO EN X
        norma_min=100000 ##VALOR PARA COMPARACION MINIMA
        while anchoinit+ancho <= ancho_end:
            recorte=img[altoinit_max:altoinit_max+alto,anchoinit:anchoinit+ancho]
            norma=np.linalg.norm(recorte,2)
            if norma<norma_min:
                norma_min=norma
                anchoinit_max=anchoinit ## Coordenada en X para maxima marca negra
            anchoinit=anchoinit+1            
        img = cv2.rectangle(img,(anchoinit_max,altoinit_max),(anchoinit_max+ancho,altoinit_max+alto),(0,255,0),1)
        
        #centro=(anchoinit_max+45,altoinit_max+20)##Calcula el centro de la marca negra
        centro=(anchoinit_max,altoinit_max+int(alto/2))

        centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+int(alto/2))

        cv2.circle(img,centro,4,(0,0,0))

        suma=0
        ##IDENTIFICA EL ESPACIO DE RESPUESTA
        ################################################################CALIFICACION DE PREGUNTA
        calificaciones_parciales=[]
        for opcion_pregunta in range(5):
             cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
             cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
             aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
             calificaciones_parciales.append(aux_calificacion)
             suma=suma+distancia_respuesta
        
        opcion_elegida=calificaciones_parciales.index(min(calificaciones_parciales)) 
        
        respuesta=FormaA.CalificacionClaseFormaA(numero_pregunta,opcion_elegida)
        print(numero_pregunta ,respuesta)



        #########################################################################################
        suma_negras+=salto_marca_negra
        altura_actual_inicial=altoinit+suma_negras
        altura_actual_final=alto_end+suma_negras
        norma_min=100000
        anchoinit=restart_ancho() ##Reestaura el ancho inicial para evitar desfases en sumatoria de barrido en X
        numero_pregunta+=1

    return img

def main():
    img=cv2.medianBlur(leer_imagen("muestraNueva.png"),3)
    rows,cols = img.shape
    M = cv2.getRotationMatrix2D((cols/2,rows/2),0.2,1)
    dst = cv2.warpAffine(img,M,(cols,rows))

    imagen=(respuestas(dst),0)
    #mostrar_imagen(imagen)
    
main()


    
